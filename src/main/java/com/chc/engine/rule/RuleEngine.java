package com.chc.engine.rule;

import com.chc.domain.Document;
import com.chc.engine.rule.action.Action;
import com.chc.engine.rule.condition.Condition;

import java.util.ArrayList;
import java.util.List;

public class RuleEngine {
    private List<Rule> rules = new ArrayList<>();
    private List<Document> documents = new ArrayList<>();

    public void loadRules(List<Rule> rules) {
        this.rules = rules;
    }

    public void applyTo(List<Document> documents) {
        this.documents = documents;
    }

    public void execute() {
        for (Document document : documents) {
            for (Rule rule : rules) {
                for (int idx = 0; idx < rule.getConditions().size(); idx++) {
                    Condition condition = rule.getConditions().get(idx);
                    boolean conditionResult = condition.validate(document);

                    if (conditionResult && condition.getActionOnTrue() != null) {
                        condition.getActionOnTrue().perform(document);
                        break;
                    } else if (!conditionResult && condition.getActionOnFalse() != null) {
                        condition.getActionOnFalse().perform(document);
                        break;
                    }
                    if (idx == rule.getConditions().size() - 1) {
                        if (conditionResult) {
                            rule.getActionsOnTrue().forEach(action -> action.perform(document));
                        } else {
                            rule.getActionsOnFalse().forEach(action -> action.perform(document));
                        }
                    }
                }
            }
        }
    }
}
