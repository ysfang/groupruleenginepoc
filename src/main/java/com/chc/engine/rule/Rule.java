package com.chc.engine.rule;

import com.chc.engine.rule.action.Action;
import com.chc.engine.rule.condition.Condition;

import java.util.ArrayList;
import java.util.List;

public class Rule {
    private List<Condition> conditions = new ArrayList<>();
    private List<Action> actionsOnTrue = new ArrayList<>();
    private List<Action> actionsOnFalse = new ArrayList<>();

    public void addCondition (Condition condition) {
        this.conditions.add(condition);
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public List<Action> getActionsOnTrue() {
        return actionsOnTrue;
    }

    public void setActionsOnTrue(List<Action> actionsOnTrue) {
        this.actionsOnTrue = actionsOnTrue;
    }

    public void addActionOnTrue(Action actionOnTrue) {
        this.actionsOnTrue.add(actionOnTrue);
    }

    public List<Action> getActionsOnFalse() {
        return actionsOnFalse;
    }

    public void setActionsOnFalse(List<Action> actionsOnFalse) {
        this.actionsOnFalse = actionsOnFalse;
    }

    public void addActionOnFalse(Action actionOnFalse) {
        this.actionsOnFalse.add(actionOnFalse);
    }
}
