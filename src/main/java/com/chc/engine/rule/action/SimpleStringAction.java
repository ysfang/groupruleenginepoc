package com.chc.engine.rule.action;

import com.chc.domain.Document;

public class SimpleStringAction implements Action {
    private String key;
    private StringActionType stringActionType;
    private String target;

    public SimpleStringAction(String key, StringActionType stringActionType, String target) {
        this.key = key;
        this.stringActionType = stringActionType;
        this.target = target;
    }

    @Override
    public void perform(Document document) {
        if (StringActionType.OVERRIDE == stringActionType) {
            System.out.println(String.format("Document id: %d, override %s: '%s' -> '%s' ", document.getAttributes().get("id"), key, document.getAttributes().get(key), target));
            document.getAttributes().put(key, target);
        } else if (StringActionType.CONCAT == stringActionType) {
            target = document.getAttributes().get(key) + target;
            System.out.println(String.format("Document id: %d, concatenate %s: '%s' -> '%s' ", document.getAttributes().get("id"), key, document.getAttributes().get(key), target));
            document.getAttributes().put(key, target);
        }
    }
}
