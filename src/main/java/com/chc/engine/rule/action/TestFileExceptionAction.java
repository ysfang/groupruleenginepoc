package com.chc.engine.rule.action;

import com.chc.domain.Document;

public class TestFileExceptionAction implements Action {
    @Override
    public void perform(Document document) {
        System.out.println(String.format("Document id: %d throws TestFileException.", document.getAttributes().get("id")));
    }
}
