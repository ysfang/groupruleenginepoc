package com.chc.engine.rule.action;

import com.chc.domain.Document;

public class FinishAction implements Action {
    @Override
    public void perform(Document document) {
        System.out.println(String.format("Document id: %d is done.", document.getAttributes().get("id")));
    }
}
