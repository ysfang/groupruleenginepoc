package com.chc.engine.rule.action;

public enum StringActionType {
    OVERRIDE, CONCAT
}
