package com.chc.engine.rule.action;

import com.chc.domain.Document;

public class InvalidProviderIdExceptionAction implements Action {
    @Override
    public void perform(Document document) {
        System.out.printf("Document id: %d throws InvalidProviderIdException." + document.getAttributes().get("id"));
    }
}
