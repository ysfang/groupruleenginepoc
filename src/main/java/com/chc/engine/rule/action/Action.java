package com.chc.engine.rule.action;

import com.chc.domain.Document;

public interface Action {
    void perform(Document document);
}
