package com.chc.engine.rule.condition;

public enum StringMatchType {
    EQUAL, BLANK, NOT_BLANK, CONTAIN, MATCH_REGEX
}
