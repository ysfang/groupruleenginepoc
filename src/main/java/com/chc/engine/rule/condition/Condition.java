package com.chc.engine.rule.condition;


import com.chc.domain.Document;
import com.chc.engine.rule.action.Action;

public interface Condition {
    boolean validate(Document document);
    Action getActionOnTrue();
    Action getActionOnFalse();
}
