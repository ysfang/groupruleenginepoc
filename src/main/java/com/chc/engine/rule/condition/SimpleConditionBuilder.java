package com.chc.engine.rule.condition;

import com.chc.engine.rule.action.Action;

public class SimpleConditionBuilder {
    public static SimpleCondition build(String key, StringMatchType stringMatchType, String target, boolean isNegated, Action actionOnTrue, Action actionOnFalse) {
        SimpleCondition sc = new SimpleCondition();
        sc.setActionOnTrue(actionOnTrue);
        sc.setActionOnFalse(actionOnFalse);
        sc.setNegated(isNegated);
        sc.setKey(key);
        sc.setStringMatchType(stringMatchType);
        sc.setTarget(target);
        return sc;
    }

    public static SimpleCondition build(String key, StringMatchType stringMatchType, String target, boolean isNegated) {
        SimpleCondition sc = new SimpleCondition();
        sc.setNegated(isNegated);
        sc.setKey(key);
        sc.setStringMatchType(stringMatchType);
        sc.setTarget(target);
        return sc;
    }
}
