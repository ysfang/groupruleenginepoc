package com.chc.engine.rule.condition;

import com.chc.domain.Document;
import com.chc.engine.rule.action.Action;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleCondition implements Condition {
    private Action actionOnTrue;
    private Action actionOnFalse;

    private boolean isNegated = false;
    private int hasEvaluated;
    private String key;
    private StringMatchType stringMatchType;
    private String target;

    @Override
    public boolean validate(Document document) {
        boolean bl = false;

        String docKey = (String) document.getAttributes().get(key);
        if (stringMatchType == StringMatchType.BLANK && StringUtils.isBlank(docKey)) {
            bl = true;
        } else if (stringMatchType == StringMatchType.MATCH_REGEX) {
            Pattern r = Pattern.compile(target);
            Matcher m = r.matcher(docKey);
            if (m.find()) {
                bl = true;
            }
        } else if (stringMatchType == StringMatchType.EQUAL && docKey.equals(target)) {
            bl = true;
        }

        hasEvaluated++;
        return (isNegated ? !bl : bl);
    }

    public Action getActionOnTrue() {
        return actionOnTrue;
    }

    public void setActionOnTrue(Action actionOnTrue) {
        this.actionOnTrue = actionOnTrue;
    }

    public Action getActionOnFalse() {
        return actionOnFalse;
    }

    public void setActionOnFalse(Action actionOnFalse) {
        this.actionOnFalse = actionOnFalse;
    }

    public boolean isNegated() {
        return isNegated;
    }

    public void setNegated(boolean negated) {
        isNegated = negated;
    }

    public int getHasEvaluated() {
        return hasEvaluated;
    }

    public void setHasEvaluated(int hasEvaluated) {
        this.hasEvaluated = hasEvaluated;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public StringMatchType getStringMatchType() {
        return stringMatchType;
    }

    public void setStringMatchType(StringMatchType stringMatchType) {
        this.stringMatchType = stringMatchType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
