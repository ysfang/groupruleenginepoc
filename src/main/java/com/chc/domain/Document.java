package com.chc.domain;

import java.util.Map;

public class Document {
    private Map attributes;

    public Map getAttributes() {
        return attributes;
    }

    public void setAttributes(Map attributes) {
        this.attributes = attributes;
    }

    public void display() {
        System.out.printf("Document id: %d - %s", attributes.get("id"), toString());
        System.out.println();
    }

    @Override
    public String toString() {
        return "Document{" +
                "attributes=" + attributes +
                '}';
    }
}
