package com.chc.domain;

import java.util.HashMap;
import java.util.Map;

public class DocumentBuilder {
    public static Document build(long id, String testFileId, String providerId, String ruleType, String deliveryType) {
        Document document = new Document();
        Map attributes = new HashMap();

        attributes.put("id", id);
        attributes.put("testFileId", testFileId);
        attributes.put("providerId", providerId);
        attributes.put("ruleType", ruleType);
        attributes.put("deliveryType", deliveryType);
        document.setAttributes(attributes);

        return document;
    }
}
