package com.chc;

import com.chc.domain.Document;
import com.chc.domain.DocumentBuilder;
import com.chc.engine.rule.Rule;
import com.chc.engine.rule.RuleEngine;
import com.chc.engine.rule.action.*;
import com.chc.engine.rule.condition.Condition;
import com.chc.engine.rule.condition.SimpleConditionBuilder;
import com.chc.engine.rule.condition.StringMatchType;

import java.util.ArrayList;
import java.util.List;

public class Impl {
    public static void main(String[] args) {
        RuleEngine engine = new RuleEngine();
        List<Document> documents = buildDocuments();

        documents.forEach(Document::display);
        engine.loadRules(buildRules());
        engine.applyTo(documents);
        engine.execute();

        documents.forEach(Document::display);
    }


    public static List<Rule> buildRules() {
        List<Rule> li = new ArrayList<>();

        li.add(lockBoxRuleBuilder());
        return li;
    }

    public static Rule lockBoxRuleBuilder() {
        Rule lockBoxRule = new Rule();
        Condition testFileIdCon = SimpleConditionBuilder.build("testFileId", StringMatchType.BLANK, null, true, new TestFileExceptionAction(), null);
        Condition providerIdCon = SimpleConditionBuilder.build("providerId", StringMatchType.MATCH_REGEX, "^1|2|3$", false, null, new InvalidProviderIdExceptionAction());
        Condition groupRuleDisableCon = SimpleConditionBuilder.build("ruleType", StringMatchType.EQUAL, "CPC", false);
        SimpleStringAction updateDeliveryType = new SimpleStringAction("deliveryType", StringActionType.OVERRIDE, "069");

        lockBoxRule.addCondition(testFileIdCon);
        lockBoxRule.addCondition(providerIdCon);
        lockBoxRule.addCondition(groupRuleDisableCon);
        lockBoxRule.addActionOnFalse(new FinishAction());
        lockBoxRule.addActionOnTrue(updateDeliveryType);
        lockBoxRule.addActionOnTrue(new FinishAction());

        return lockBoxRule;
    }

    public static List<Document> buildDocuments() {
        List<Document> li = new ArrayList<>();
        li.add(DocumentBuilder.build(1, "test-file-001", "", "", null));
        li.add(DocumentBuilder.build(2, "", "1", "CPC", null));
        li.add(DocumentBuilder.build(3, "", "1", "", null));

        return li;
    }
}
